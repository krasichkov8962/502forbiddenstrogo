<?php

namespace App\Repositories;


use App\Models\CoreModel;
use App\Traits\Filterier;
use App\Traits\Paginatier;
use Illuminate\Database\Eloquent\Collection;

abstract class CoreRepository
{

    use Filterier, Paginatier;

    abstract protected function getModel(): CoreModel;

    abstract protected function getFilterRules(): array;


    public function getByIdForPublic(int $id, $lang = 'ru'): CoreModel
    {
        if ($lang == 'eng') {
            $model = $this->getModel()->forPublicEng()->findOrFail($id);
        } elseif ($lang == 'admin') {
            $model = $this->getModel()->forAdmin()->findOrFail($id);
        } else {
            $model = $this->getModel()->forPublic()->findOrFail($id);
        }

        if (method_exists($this, 'incrementModelViews')) {
            $this->incrementModelViews($model);
        }

        return $model;
    }

    public function getByIdForAdmin(int $id): CoreModel
    {
        $model = $this->getModel()->forAdmin()->findOrFail($id);

        return $model;
    }

    public function getAllForPublic(bool $paginated = false, $request_for_filter = null, $local = 'ru', array $exclude = [])//: Collection
    {
        if ($local == 'eng') {
            $query_builder = $this->getModel()->forPublicEng();
        } elseif ($local == 'admin') {
            $query_builder = $this->getModel()->forAdmin();
        } else {
            $query_builder = $this->getModel()->forPublic();
        }

        if (method_exists($this, 'getOrder')) {
            $order = $this->getOrder();
            $query_builder = $query_builder->orderBy($order['column'], $order['type']);
        } else {
            $query_builder = $query_builder->orderBy('id', 'DESC');
        }

        if ($request_for_filter && count($this->getFilterRules())) {
            $query_builder = $this->filter($query_builder, $request_for_filter, $this->getFilterRules());
        }

        if ($request_for_filter && $request_for_filter->id) {
            if (intval($request_for_filter->id)) {
                $query_builder = $query_builder->where('id', '!=', $request_for_filter->id);
            } else {
                $query_builder = $query_builder->where('slug', '!=', $request_for_filter->id);
            }
        }

        if ($exclude && count($exclude)) {
            $query_builder = $query_builder->where($exclude['column'], '!=', $exclude['value']);
        }

        if ($paginated) {
            $query_builder = $this->paginate($query_builder);
        }

        return $query_builder->get();
    }

    public function getAllForAdmin()
    {
        $query_builder = $this->getModel()->forAdmin();

        if (method_exists($this, 'getOrder')) {
            $order = $this->getOrder();
            $query_builder = $query_builder->orderBy($order['column'], $order['type']);
        } else {
            $query_builder = $query_builder->orderBy('id', 'DESC');
        }

        return $query_builder->get();
    }

    public function getCountForPagination($request_for_filter = null): int
    {
        $query_builder = $this->getModel()->select(['id']);

        if ($request_for_filter && count($this->getFilterRules())) {
            $query_builder = $this->filter($query_builder, $request_for_filter, $this->getFilterRules());
        }

        return $query_builder->get()->count();
    }

    public function getForAllPagination(int $page = 1, int $limit = 3)
    {
        return $this->getModel()->forPublic()->limit($limit)->offset($page * $limit)->get();
    }

}

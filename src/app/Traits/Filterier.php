<?php

namespace App\Traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait Filterier
{

    protected function filter(Builder $query_builder, Request $request, array $rules = []): Builder
    {
        if ($key = array_search('systemTagList', array_column($rules, 'parameter'))) {
            if ($value = $request->input($rules[$key]['parameter'])) {
                $field = $rules[$key]['fields'][0];

                $query_builder->where(function ($query) use ($field, $value) {
                    $query->whereJsonContains($field, $value);
                });
            }
        }

        $query_builder->where(function ($query) use ($request, $rules) {
            foreach ($rules as $rule) {
                if ($value = $request->input($rule['parameter'])) {
                    foreach ($rule['fields'] as $field) {
                        if ($rule['parameter'] !== 'systemTagList') {
                            switch ($rule['comparison']) {
                                case 'like':
                                    $query->orWhereLike($field, $value);
                                    break;
                                case 'ilike':
                                    $query->orWhereIlike($field, $value);
                                    break;
                                case 'like_json':
                                    $query->orWhere(
                                        DB::raw("lower('$value')"),
                                        '=',
                                        DB::raw("any(select lower(jsonb_array_elements_text(\"$field\"::jsonb)))")
                                    );
                                    break;
                                case 'year':
                                    $query->orWhere(DB::raw("EXTRACT(YEAR from $field)"), $value);
                                    break;
                                case 'in':
                                    $query->orWhereIn($field, explode(',', $value));
                                    break;
                                default:
                                    $query->orWhere($field, $rule['comparison'], $value);
                                    break;
                            }
                        }
                    }

                }
            }
        });

        return $query_builder;
    }

}
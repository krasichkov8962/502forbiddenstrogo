<?php

namespace App\Traits;

use App\Models\EventLog;
use App\Models\UsersModel;
use Illuminate\Database\Eloquent\Model;

trait Observable
{
    public static function bootObservable()
    {
        static::saved(function (Model $model) {
            self::createEventLog($model, 'save');
        });
        static::created(function (Model $model) {
            self::createEventLog($model, 'create');
        });
        static::updated(function (Model $model) {
            self::createEventLog($model, 'update');
        });
    }

    public static function createEventLog(Model $model, string $eventType = 'save')
    {
        try {
            if (request()->has('user_id')) {
                $user = UsersModel::find(request()->get('user_id'));
                if ($user) {
                    EventLog::create([
                        'event_type' => $eventType,
                        'object_type' => 'model',
                        'object_name' => $model->table,
                        'object_id' => $model->id,
                        'user_name' => $user->full_name ?? '',
                        'user_id' => request()->get('user_id'),
                        'ip' => request()->ip()
                    ]);
                }
            }
        } catch (\Exception $e) {
            var_dump($e);
        }
    }
}

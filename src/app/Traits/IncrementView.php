<?php

namespace App\Traits;


use App\Models\CoreModel;

trait IncrementView
{

    protected function incrementModelViews(CoreModel $model)
    {

        $model::withoutEvents(function () use ($model) {
            $model->view_count++;
            $model->save();
        });
    }

}

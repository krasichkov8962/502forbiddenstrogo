<?php

namespace App\Models;


class LiveStream extends CoreModel
{
    protected $table = 'live_stream';

    public $timestamps = false;

    protected $casts = [
        'start_date_time' => 'datetime:Y-m-d\TH:i:s',
        'end_date_time' => 'datetime:Y-m-d\TH:i:s',
    ];

    private const PREVIEWS_STORAGE = 'storage/images/live_stream_previews/';

    private static $public_fields = [
        'id',
        'start_date_time',
        'end_date_time',
        'preview_media',
        'live_stream',
        'title',
        'description',
        'text',
        'tags_list',
        'rubric',
        'view_count'
    ];

    private static $public_fields_eng = [
        'id',
        'start_date_time',
        'end_date_time',
        'preview_media',
        'live_stream',
        'title_eng',
        'description_eng',
        'text_eng',
        'tags_list',
        'rubric',
        'view_count'
    ];

    private static $admin_fields = [
        'title_eng',
        'description_eng',
        'text_eng'
    ];

    public static function getPublicFields()
    {
        return self::$public_fields;
    }

    public static function getPublicEngFields()
    {
        return self::$public_fields_eng;
    }

    public static function getAdminFields()
    {
        return array_merge(self::$public_fields, self::$admin_fields);
    }

    public function rubric_model()
    {
        return $this->hasOne('App\Models\RubricModel', 'id', 'rubric');
    }

    public function getPreviewMediaAttribute($value)
    {
        if (is_null($value)) {
            return null;
        } else {
            return url(self::PREVIEWS_STORAGE . $value);
        }
    }

    public function getTagsListAttribute($value)
    {
        return json_decode($value);
    }
}

<?php

namespace App\Models;


class Nomination extends CoreModel
{

    protected $table = 'nominations';

    public $timestamps = false;

    private const IMAGES_STORAGE = 'storage/images/nomination_images/';

    private static $public_fields = ['id', 'title', 'description', 'image'];

    private static $admin_fields = [];

    public static function getPublicFields()
    {
        return self::$public_fields;
    }

    public static function getAdminFields()
    {
        return array_merge(self::$public_fields, self::$admin_fields);
    }

    public function getImageAttribute($value)
    {
        if (is_null($value)) {
            return null;
        } else {
            return url(self::IMAGES_STORAGE . $value);
        }
    }

}
